# zedtech-gtce 0.5.1

This is a fork of Zederrian Technology for GregTech Community Edition, with additional textures and models created by [Puzzlebark](https://www.curseforge.com/members/puzzlebark).

Zederrian Technology was created by [Zerrens](https://forum.industrial-craft.net/core/user/12229-zerrens/), and the original GT5U version of the resource pack can be found [here](https://github.com/MCTian-mi/Zederrian-Technology-GT5U/).

## Progress

| Mod Name             |   Status   |
| -------------------- | :--------: |
| appliedenergistics2  | Working on |
| forestry             |  Planned   |
| gregtech             | Working on |
| gregicality          | Working on |
| railcraft            | Working on |

## License

[CC BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en)

[Screenshot of the license](https://i.imgur.com/3QeuL49.png)

